'''
Change Return Program - The user enters a cost and then the amount of money given.
The program will figure out the change and the number of quarters, dimes, nickels, pennies needed for the change.
All the vallues are calculated in Swedish Crowns SEK
'''


'''
Let's do the simplified version first and then we do it with classes
'''


'''
INTENTED FOR LATER USE:

count1000 = 0
count500 = 0
count200 = 0
count100 = 0
count50 = 0
count20 = 0
count10 = 0
count5 = 0
count2 = 0
count1 = 0
'''

cost = int(input('How much does this cost? \n'))

cash_received = int(input('What would you like to pay with? \n'))

difference = cash_received - cost

change = ''

while difference != 0:
    if difference >= 1000:
        while difference >= 1000:
            difference -= 1000
            change +='x1000+ '
    elif difference >= 500:
        while difference >= 500:
            difference -= 500
            change +='x500+ '
    elif difference >= 200:
        while difference >= 200:
            difference -= 200
            change +='x200+ '
    elif difference >= 100:
        while difference >= 100:
            difference -= 100
            change +='x100+ '
    elif difference >= 50:
        while difference >= 50:
            difference -= 50
            change +='x50+ '
    elif difference >= 20:
        while difference >= 20:
            difference -= 20
            change +='x20+ '
    elif difference >= 10:
        while difference >= 10:
            difference -=10
            change +='x10+ '
    elif difference >= 5:
        while difference >= 5:
            difference -= 5
            change +='x5+ '
    elif difference >= 2:
        while difference >= 2:
            difference -= 2
            change +='x2+ '
    elif difference >= 1:
        while difference >= 1:
            difference -=1
            change +='x1+ '

print('Here comes your change '+change +' :)')
